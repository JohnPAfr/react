class Home extends React.Component {
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this)
        this.input = React.createRef()
    }

    handleClick(e) {
        console.log(this.input.value)
    }

    render() {
        return <div>
            <input type="text" ref={this.input}/>
            <button onClick={this.handleClick}>Submit</button>
        </div>
    }
}

ReactDOM.render(<Home/>, document.getElementById('app'))