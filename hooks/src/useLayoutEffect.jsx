import React, { useLayoutEffect, useState } from 'react';
import { render } from 'react-dom';

function App() {

    const [count, setCount] = useState(0);

    const button = useRef(null);

    useLayoutEffect(() => {
        if (count % 2 === 0) {
            button.current.style.color = 'lightgreen';
        } else {
            button.current.style.color = 'lightred';
        }
    })

    const handleClick = useCallback(() => {
        setCount(c => c + 1)
    }, []);

    return <button onClick={ handleClick } ref={ button }>Incrémenter</button>
}

render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)