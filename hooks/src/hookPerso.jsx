import React, { useEffect, useState } from 'react';
import { render } from 'react-dom';
//import regeneratorRuntime from "regenerator-runtime";

function useIncrement(initial = 0, step = 1) {
    const [count, setCount] = useState(initial);

    const increment = () => setCount(c => c + step);

    return [count, increment];
}

function useAutoIncrement(initial = 0, step = 1) {
    const [count, increment] = useIncrement(initial, step);

    useEffect(() => {
        const timer = window.setInterval(() => {
            increment()
            console.log('hello');
        }, 1000);
        return function() {
            clearInterval(timer);
        };
    }, [])

    return count;
}

function useToggle(initial = true) {
    const [compteurVisible, setCompteurVisible] = useState(initial);

    const toggleCompteur = () => {
        setCompteurVisible(cptVisible => !cptVisible)
    }

    return [compteurVisible, toggleCompteur]
}

function useFetch(url) {
    const [state, setState] = useState({
        items: [],
        loading: true
    });
    //const [loading, setLoading] = useState(true);

    useEffect(function() {
        (async function() {
            const response = await fetch(url);
            const responseData = await response.json();
            if (response.ok) {
                setState({ items: responseData, loading: false} );
            } else {
                alert(JSON.stringify(responseData));
                setState({ items: [], loading: false} );
            }
        })()
    }, []);

    return [state.loading, state.items];
}

function Compteur() {
    // const [count, increment] = useIncrement(10, 2);
    const count = useAutoIncrement(10, 2);

    return <button>Incrémenter { count }</button>
}

function CommentTable() {
    const [loading, comments] = useFetch('https://jsonplaceholder.typicode.com/comments?_limit=10');
    console.log(comments);
    if(loading)
        return 'Chargement...';
    else
        return <table>
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Body</td>
                </tr>
            </thead>
            <tbody>
                { comments.map(c => {
                    return <tr key={ c.id }>
                        <td>{ c.name }</td>
                        <td>{ c.email }</td>
                        <td>{ c.body }</td>
                    </tr>
                }) }
            </tbody>
        </table>;
}

function TodoList() {
    const [todos, setTodos] = useState([]);
    const [loading, setLoading] = useState(true);

    /* 
     * Lors d'un appel ajax, pour éviter de retourner une Promise avec await
     * on utilise une fonction qui s'autocall
    */ 
    useEffect(function() {
        (async function() {
            const response = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=10');
            const responseData = await response.json();
            if (response.ok) {
                setTodos(responseData);
                setLoading(false);
            } else {
                alert(JSON.stringify(responseData));
            }
        })()
    }, []);

    if(loading) {
        return 'Chargement...';
    }
    return <ul>
        { todos.map(todo => <li key={ todo.id }>{ todo.title }</li>) }
    </ul>;
}

function App() {

    const [compteurVisible, toggleCompteur] = useToggle(false);

    return <div>
        Afficher le compteur 
        <input type="checkbox" onChange={ toggleCompteur } checked={ compteurVisible }/>
        <br/>
        { compteurVisible && <Compteur /> }
        <TodoList />
        <CommentTable />
    </div>
}

render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)