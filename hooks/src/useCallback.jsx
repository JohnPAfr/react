import React, { useState, useMemo, useCallback } from 'react';
import { render } from 'react-dom';

const Button = React.memo(({ onClick }) => {
    return <button onClick={ onClick }>Bonjour</button>
})

function App() {
    const [count, setCount] = useState(0);

    const handleClick = useCallback(() => {
        alert('Bonjour ' + count);
    }, [count]);

    return <div>
        <Button onClick={ handleClick } />
        <button onClick={ () => setCount(c => c + 1) }>Incrémenter</button>
    </div>;
}

render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)