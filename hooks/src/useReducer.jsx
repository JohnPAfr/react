import React, { useReducer } from 'react';
import { render } from 'react-dom';

function init(initialValue) {
    return {count: initialValue};
}

function reducer(state, action) {
    switch(action.type) {
        case 'increment':
            return {count: state.count + 1};
        case 'decrement':
            if (state.count === 0)
                return state;
            return {count: state.count - 1};
        case 'reset':
            return init(0);
        default:
            throw new Error('error');
    }
}

function App() {
    const [count, dispatch] = useReducer(reducer, 0, init);

    return <div>
        <div>Count : {JSON.stringify(count)}</div>
        <button onClick={() => dispatch({type: 'increment'})}>Incrémenter</button>
        <button onClick={() => dispatch({type: 'decrement'})}>Décrementer</button>
        <button onClick={() => dispatch({type: 'reset'})}>Reset</button>
    </div>
}

render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)