import React, { useState, useEffect } from 'react';
import { render } from 'react-dom';

// Hook personnalisé

function useIncrement(init = 0, step = 1) {
    const [count, setCount] = useState(init);

    const increment = () => {
        setCount(c => c + step)
    }

    return [count, increment];
}

function Compteur() {
    const [count, increment] = useIncrement(10, 2);
    
    useEffect(() => {
       const timer = window.setInterval(() => {
           console.log('hello')
            increment()
        }, 1000);
        return function() {
            clearInterval(timer)
        }
    }, [])

    return <button onClick={ increment }>{ count }</button>;
}

render(
    <div>
        <Compteur />
    </div>,
    document.getElementById('app')
)

window.setTimeout(() => {
    render(
        <div>
        </div>,
        document.getElementById('app')
    )
}, 5000)