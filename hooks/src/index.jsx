import React, { createContext, useCallback, useContext, useMemo, useState } from 'react';
import { render } from 'react-dom';

const FormCtx = createContext({})

function FormContext({defaultValue, onSubmit, children}) {
    const [data, setData] = useState(defaultValue);

    const handleFieldChange = useCallback((name, value) => {
        setData(d => ({...d, [name]: value}));
    });

    const value = useMemo(() => ({...data, handleFieldChange}), [data, handleFieldChange]);

    const onFormSubmit = useCallback((e) => {
        e.preventDefault() // to see value in console
        onSubmit(value)
    }, [onSubmit, value])

    return <FormCtx.Provider value={value}>
        <form onSubmit={onFormSubmit}>
            {children}
        </form>
    </FormCtx.Provider>
}

function FormField({name, children}) {

    const data = useContext(FormCtx);

    const handleChange = useCallback((e) => {
        data.handleFieldChange(e.target.name, e.target.value)
    }, [data.handleFieldChange])

    return <div className="form-group">
        <label htmlFor={name}>{children}</label>
        <input type="text" className="form-control" name={name} id={name} value={data[name] || ''} onChange={handleChange}/>
    </div>
    
}

function PrimaryButton({children}) {
    return <button className="btn btn-primary">{children}</button>
}

function App() {

    const handleSubmit = useCallback(function(value) {
        console.log(value)
    }, [])

    return <div className="container">
        <FormContext defaultValue={{firstname: 'Doe', lastname: 'John'}} onSubmit={handleSubmit}>
            <FormField name='firstname'>Nom</FormField>
            <FormField name='lastname'>Prénom</FormField>
            <PrimaryButton>Envoyer</PrimaryButton>
        </FormContext>
    </div> 
}


render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)