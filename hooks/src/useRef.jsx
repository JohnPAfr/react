import React, { useRef } from 'react';
import { render } from 'react-dom';

function App() {
    
    const input = useRef(null);

    return <div>
        <input type="text" ref={ input }/>
    </div>
}

render(
    <div>
        <App />
    </div>,
    document.getElementById('app')
)